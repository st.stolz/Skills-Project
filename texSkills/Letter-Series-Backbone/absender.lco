%%
% Deutsche LaTeX Briefvorlage 
% erstellt von Stefan Stolz <st.stolz@gmail.com>
% Jänner 2019
%%

\ProvidesFile{absender.lco}[letter-class-option file]

% Symbole für Handy, Telefon, E-Mail, ...
\RequirePackage{marvosym}

% Kodierung des Inhaltes dieser Datei
\RequirePackage[utf8]{inputenc}

% Elemente des Briefes aktivieren und deaktivieren (%)
\KOMAoptions{
    foldmarks=true,
    fromalign=right,
    fromrule=afteraddress,
    footsepline=off,
    fromurl=true,
    fromemail=true,
    %fromfax=true,
    fromphone=true,
    fromlogo=true
}

% Absenderdaten:
\setkomavar{fromname}{Mag. Max Mustermann, MSc}
\setkomavar{fromlogo}{\parbox[b]{8cm}{\usekomafont{fromaddress}%
        {\mbox{\LARGE \bfseries Mag. Max Mustermann, MSc}}
        \smallskip}
}

\setkomavar{fromphone}[\Mobilefone~]{+43\,681\,83992302}
\setkomavar{fromemail}[\Letter~]{example@gmail.com}
\setkomavar{fromurl}[]{https://gitlab.com/st.stolz}
\setkomavar{backaddress}{Mag. Max Mustermann, Beispielweg 67, 9847 Exampletown}
\setkomavar{fromaddress}{Beispielweg 67\\9847 Exampletown}
%\setkomavar{fromfax}[\Faxmachine~]{+49\,32\,8392495}

\setkomavar{signature}{Max Mustermann}
\renewcommand*{\raggedsignature}{\raggedright}

\endinput