# GNS3 Docker Projekt mit Webserver, Router und NAT

Ein importfähiges GNS3 Projekt, welches mittels Docker Container ein Netzwerk
mit Webserver, Router und NAT aufsetzt.

Topologie:<br>
![Topologie](.images/Topologie-Nat-Webserver-Router.png)

## Verwendete Technologien

### Voraussetzungen

Voraussetzung für den Import ist eine konfigurierte GNS3 VM. Wie diese eingerichtet wird, kann
[in der Skills Wiki nachgelesen werden](https://gitlab.com/st.stolz/Skills-Project/wikis/gns3Skills/GNS3-docker-Einrichtung).

### Verwendete Docker Container 

1. `gns3/webterm` für einen Client mit Firefox Browser
2. `gns3/ubuntu:xenial` für einen Ubuntu Client mit den wichtigsten Netzwerk Tools
3. `registry.gitlab.com/st.stolz/skills-project/gns3-router` für routing
   vorbereiteter Ubuntu Server
4. `registry.gitlab.com/st.stolz/skills-project/gns3-webserver` ein Ubuntu
   Server mit laufendem Apache Webserver

## Bekannte Probleme

### Import bricht ab, weil Server scheinbar nicht reagiert

GNS3 kann beim Import der gns3project Datei abbrechen, weil es glaubt, dass der
Server nicht reagiert. Tatsächlich dauert nur das Downloaden der Container etwas
länger.

Abhilfe verschafft, die Container manuell in GNS3 hinzuzufügen und in ein
dummy Projekt zu ziehen, damit sie heruntergeladen werden. Ebenfalls sollte
funktionieren, den Import mehrmals auszuführen.