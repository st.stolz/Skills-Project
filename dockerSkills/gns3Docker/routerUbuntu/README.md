# GNS3 Router

Ein basaler docker Router Container für ein GNS3 Netzwerk. Aufbauend auf einem Ubuntu
Container.

Im Container wird automatisch Routing und Masquerading (NAT) aktiviert. 

## Docker Pull Command

```bash
docker pull registry.gitlab.com/st.stolz/skills-project/gns3-router
```