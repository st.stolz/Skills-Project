#!/bin/bash

echo 1 | tee /proc/sys/net/ipv4/ip_forward &> /dev/null
# sysctl -w net.ipv4.ip_forward=1
iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
exec bash
