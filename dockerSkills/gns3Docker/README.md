# Docker für GNS3

Hier werden Docker gesammelt, die für GNS3 verwendet werden. Die docker images
werden automatisch per CI/CD erstellt. 

Um GNS3 für Docker einzurichten, gibt es in der WIKI des Projektes [eine Anleitung](https://gitlab.com/st.stolz/Skills-Project/wikis/gns3Skills/GNS3-docker-Einrichtung).

Für die Pfade, auf denen sie bereitgestellt werden, siehe [Container
Registry](https://gitlab.com/st.stolz/Skills-Project/container_registry) des
Projektes. Vorausgesetzt, Docker ist korrekt in GNS3 eingerichtet, können sie
ganz leicht eingebunden werden. Einfach unter "Edit" - "Preferences" - "Docker
Containers" - "New" den entsprechenden Pfad hinzufügen.

Beispiel:<br>
![New Docker in GNS3](.images/GNS3-New&#32;Docker&#32;VM&#32;template.png)