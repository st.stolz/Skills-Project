# GNS3 Webserver

Ein basaler docker Webserver für ein GNS3 Netzwerk. Aufbauend auf einem Ubuntu
Container.

Im Container wird ein Apache Webserver installiert und auf Port 80 bereit gestellt.

## Docker Pull Command

```bash
docker pull registry.gitlab.com/st.stolz/skills-project/gns3-webserver
```