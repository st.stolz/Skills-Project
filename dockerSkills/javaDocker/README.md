# Docker Container for Java execution


Build the container the following way.

```bash
docker build -t stst/java .
```

cd to the src folder of your Java project. Then execute the following Bash command.

```bash
docker run --rm -v $PWD:/usr/src/myapp stst/java JavaFile.java
```

## MYSQL Connection

It is also possible to connect to a MYSQL database. But remember that in this case the docker container tries to connect to the database. So it is not possible to access a database on localhost. 

You have to use `--network` in the `docker run` command to connect to a docker network with a MYSQL Server. For the host you have to use the container name. For Example: `docker run --network dbdocker_default --rm -v $PWD:/usr/src/myapp stst/java JavaFile.java`. 

You can list existing networks with `docker network ls`