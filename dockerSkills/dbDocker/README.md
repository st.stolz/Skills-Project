# Datenbanken mit Frontends für DBI Unterricht unter docker

Warum für die Installation der Datenbanken docker verwenden und nicht nativ unter Windows oder Linux installieren?

1. **Development** - Alle Entwickler können auf einfache Weise exakt die selbe Entwicklungsumgebung
   verwenden. Ein Dockerfile mit der Beschreibung des Containers oder eine
   docker-compose.yml sind alles, was man dafür braucht. Per Continous
   Integration von Providern wie GitLab kann das Image sogar automatisch bei
   jedem Push oder Tagging gebuildet werden.
2. **Nativ** - So gut wie alle großen Server Anwendungen, die nicht aus dem Hause Microsoft stammen, werden in einer Unix Umgebung entwickelt und danach bestenfalls auf die Windows Umgebung portiert. Bei der Verwendung von Docker verwenden wir also das native System wie es entwickelt wird und sind nicht abhängig von einer korrekten Portierung.
3. **Portability** - durch die Verwendung von docker läuft unser System ohne weiteren Anpassungen auf jedem Betriebssystem. Wir schaffen uns also in Entwicklung und Produktivumgebung keine unnötigen Abhängigkeiten und Einschränkungen.
4. **Einfachheit** - Da die Anwendung ihre eigene Betriebssystem Umgebung mitliefert, wird Konfigurationsarbeit auf ein Minimum beschränkt.
5. **Scalability** - Neue Serveranwendungen können hinzugefügt werden, ohne die Bestehenden auch nur geringfügig zu beeinflussen.

## Voraussetzungen

Eine funktionierende Docker Installation. Die folgende Anleitung bezieht sich,
auf eine [Docker Toolbox](https://docs.docker.com/toolbox/toolbox_install_windows/) Installation, sollte aber mit der Standardversion
genauso funktionieren.

## Die Docker Composition starten

Wie man eine Docker Composition installiert und startet, wird [im WIKI erklärt](https://gitlab.com/st.stolz/Skills-Project/wikis/dockerSkills/Docker-Start-Composition).

Tipp für Linux: im Ordner der docker-compose.yml Datei werden die Ordner `www` und `mysqlBackup` angelegt. Diese werden mit root Rechten erzeugt. Wenn du künftig darauf Zugriff haben willst, reicht es `sudo chmod 770 mysqlBackup www` auszuführen.

## Verwendung der Container

Wenn die Composition gestartet ist, kann im Browser die ip der Maschine verwendet werden, um auf den Webserver zuzugreifen. Wenn Docker direkt unter Linux oder mit WSL2 installiert wurde, greift man über localhost zu. Ansonsten ist es die IP der virtuellen Maschine. Die Ports können aus der docker-compose.yaml Datei heraus gelesen werden. Siehe dazu die `ports` Angabe. Der Wert links vom Doppelpunkt ist der Port, der für den Host verwendet wird. 

## Zustand (State)

Achtung! Die Container sind stateless.
Solange kein Update durchgeführt wird oder sie removed werden, bleiben die Daten erhalten.
Ansonsten gehen sie verloren. `docker-compose stop` und `docker-compose start` erhält die Daten.
Mit `docker-compose down` gehen alle Daten verloren.

Will man die Datenbanken sichern, empfiehlt es sich, dumps durchzuführen. Für MYSQL würde das etwa so ausschauen:

```bash
# Backup
docker exec mysql /usr/bin/mysqldump -u root --password=123 --all-databases --skip-lock-tables > backup.sql

# Restore
cat backup.sql | docker exec -i mysql /usr/bin/mysql -u root --password=123
```

`mysql` ist dabei der Name des Containers (hier "mysql").

## Tipps

Auf die command line einer docker Maschine kommst du mit diesem Befehl:

```bash
# laufende Container anzeigen
docker container ls
# in einen Container per bash einloggen
docker exec -it CONTAINER /bin/bash
# im Container den mysql client starten
mysql -uroot -p123
```
